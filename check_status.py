# @Time     :   2021-3-26
# @Author   :   mikeyy
# @Desc     :   Used to check screenshot_box.py output

import os

WORKING_ROOT = '/home/mikeyy/repo/webis-segment/web-segment-algo'

documents = os.listdir(os.path.join(WORKING_ROOT, 'webis-webseg-20'))
documents.sort()

output = []
for doc in documents:
    WORKING_DIR = os.path.join(WORKING_ROOT, 'webis-webseg-20', doc)
    if not os.path.isfile(os.path.join(WORKING_DIR, 'seg_info.json')):
        output.append(doc)

print(len(output))
print(output)