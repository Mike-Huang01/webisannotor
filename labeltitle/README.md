# labelhtml

* 标注网页title，后台程序

## 使用
* 初始化数据库 `python manage.py migrate`
* 变更数据库后 `python manage.py makemigrations` 然后再运行migrate
* 创建admin `python manage.py createsuperuser`
* 公网运行 `pytho manage.py runserver 0.0.0.0:8000`

## 接口

* / 根据数据库标记位置，呈现网页
* /prev 当前位置往前一个网页id
* /next 当前位置往后一个网页id
* /reset 返回标记offset
