from django.contrib import admin
from .models import (
    UserProfile, Record, 
    Page, Page_content, Page_selected)

# Register your models here.
@admin.register(Page_content)
class PageContentAdmin(admin.ModelAdmin):
    list_display = ('page_id', 'block_id')
    fields       = ('page_id', 'block_id')

@admin.register(Page_selected)
class PageSelectedAdmin(admin.ModelAdmin):
    list_display = ('page_id', 'selected')
    fields       = ('page_id', 'selected')

@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'offset', 'total')
    fields       = ('user', 'offset', 'total')

@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    list_display = ('page_id', 'block_id', 'xpath', 'user')
    fields       = ('page_id', 'block_id', 'xpath', 'user')

@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('page_id', 'blocks', 'label_blocks', 'desc', 'user')
    fields       = ('page_id', 'blocks', 'label_blocks', 'desc', 'user')