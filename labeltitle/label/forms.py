from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=50, widget=forms.TextInput(attrs={'name' : 'username', 'class' : 'form-control', 'placeholder': 'username'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'name' : 'username', 'class' : 'form-control', 'placeholder' : 'username'}))

    