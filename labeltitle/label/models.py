from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    offset = models.PositiveIntegerField(default=0, verbose_name='起始任务')
    total = models.PositiveIntegerField(default=1, verbose_name='任务总数')

    class Meta:
        verbose_name = 'User Profile'
    
    def __str__(self):
        return self.user.__str__()

class Record(models.Model):
    page_id  = models.CharField(max_length=10, verbose_name='页ID')
    block_id = models.IntegerField(verbose_name='块ID')
    xpath    = models.CharField(max_length=100, verbose_name='Xpath', blank=True)
    user     = models.ForeignKey(UserProfile, default=None, verbose_name='标记者', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Record'
        ordering = ['page_id']

class Page(models.Model):
    page_id     = models.CharField(max_length=10, verbose_name='页ID')
    blocks      = models.IntegerField(verbose_name='块数')
    label_blocks= models.IntegerField(verbose_name='标记数')
    desc        = models.CharField(max_length=200, verbose_name='描述', blank=True)
    user     = models.ForeignKey(UserProfile, default=None, verbose_name='标记者', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Page'
        ordering     = ['page_id']

class Page_selected(models.Model):
    SELECTED_CHOINCE = (
        (0, 'not selected'),
        (1, 'selected')
    )

    page_id  = models.CharField(max_length=10, verbose_name='页ID')
    selected = models.IntegerField(choices=SELECTED_CHOINCE, verbose_name='选择')
    user     = models.ForeignKey(UserProfile, default=None, verbose_name='标记者', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Page selected'
        ordering     = ['page_id']

class Page_content(models.Model):
    page_id  = models.CharField(max_length=10, verbose_name='页ID')
    block_id = models.IntegerField(verbose_name='块ID')
    user     = models.ForeignKey(UserProfile, default=None, verbose_name='标记者', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Page content'
        ordering     = ['page_id']

    



