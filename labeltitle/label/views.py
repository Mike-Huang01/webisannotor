import os
import json
import urllib

from django.contrib import auth
from django.urls import reverse
from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required

from .forms import LoginForm
from .models import UserProfile, Record, Page, Page_content, Page_selected
from django.conf import settings

# Create your views here.

def get_seginfo(pos):
    id = settings.IDS[pos]
    with open(os.path.join(settings.STATICFILES_DIRS[1], id, 'seg_info.json')) as f:
        seg_info = json.load(f)

    msg = []
    for i in range(len(seg_info)):
        seg = seg_info[i]
        node_set = {}
        node_set['id'] = i
        node_set['node'] = []
        for node in seg:
            if node.get('node') != None and ( not 'IMG' in node['node']['xpath']):
                node_set['node'].append({'text': node['node']['text'][:25], 'xpath' : node['node']['xpath']})
            elif node.get('node') != None:
                 node_set['node'].append({'text' : node['node']['xpath'], 'xpath' : node['node']['xpath']})
        msg.append(node_set)
    return msg

def get_pos(user):
    offset = Page_selected.objects.filter(user=user).count()
    return offset

    

@login_required
def index(request):
    user   = UserProfile.objects.get(user=request.user)
    offset = get_pos(user)
    request.session['pos'] = offset

    msg = get_seginfo(offset)
    context = {
        'pos'     : offset,
        'seg_msg' : msg,
        'num'     : len(msg)
    }

    return render(request, 'index.html', context=context)

@login_required
def reset(request):
    user   = UserProfile.objects.get(user=request.user)
    offset = get_pos(user)
    request.session['pos'] = offset
    msg = get_seginfo(offset)

    return JsonResponse({'pos' : request.session['pos'],
                         'seg_msg' : msg})
@csrf_exempt
@require_POST
@login_required
def skip(request):
    data = dict(request.POST.items())
    offset = int(data['pos'])
    request.session['pos'] = offset
    msg = get_seginfo(offset)

    return JsonResponse({'pos' : request.session['pos'],
                         'seg_msg' : msg})


@login_required
def next(request):
    if not request.session.get('pos'):
        user   = UserProfile.objects.get(user=request.user)
        offset = get_pos(user)
        request.session['pos'] = offset
        
    request.session['pos'] += 1
    msg = get_seginfo(request.session['pos'])

    return JsonResponse({'pos' : request.session['pos'],
                         'seg_msg' : msg})

@login_required
def prev(request):
    if not request.session.get('pos'):
        user   = UserProfile.objects.get(user=request.user)
        offset = get_pos(user)
        request.session['pos'] = offset

    request.session['pos'] -= 1
    msg = get_seginfo(request.session['pos'])
    
    return JsonResponse({'pos' : request.session['pos'],
                         'seg_msg' : msg})

@csrf_exempt
@require_POST
def label(request):
    user = UserProfile.objects.get(user=request.user)
    data = dict(request.POST.items())
    sel_data = data['sel_data']
    if sel_data == '':
        page_selected = Page_selected.objects.create(page_id=data['page_id'],
                                                     selected=0,
                                                     user=user)
        page_selected.save()
    else:
        page_selected = Page_selected.objects.create(page_id=data['page_id'],
                                                     selected=1,
                                                     user=user)
        page_selected.save()
        content_pairs = sel_data.split('&')
        for pair in content_pairs:
            
            eles = pair.split('=')
            if eles[0] == 'selected':
                continue

            if len(eles) < 2:
                print(pairs)
                print(eles)
                raise Exception('ele wrong!')
            
            content_record = Page_content.objects.create(page_id=data['page_id'],
                                                        block_id=eles[0],
                                                        user=user)
            content_record.save()
        


    page = Page.objects.create(page_id=data['page_id'],
                               blocks=data['blocks'],
                               label_blocks=data['label_blocks'],
                               desc=data['desc'],
                               user=user)

    page.save()

    form_data = data['form_data']
    if form_data == '':
        return JsonResponse({'msg' : 'success'})
    
    pairs = form_data.split('&')
    
    for pair in pairs:
        eles = pair.split('=')
        if len(eles) < 2:
            print(pairs)
            print(eles)
            raise Exception('ele wrong!')

        record = Record.objects.create(page_id=data['page_id'],
                                       block_id=eles[1],
                                       xpath=urllib.parse.unquote(eles[0]),
                                       user=user)
        
        record.save()

    return JsonResponse({'msg' : 'success'})


def login(request):
    if request.method != 'POST':
        form = LoginForm()
        return render(request, 'login.html', {'form' : form})
    
    form = LoginForm(request.POST)
    context = {
        'form' : form,
        'message' : '密码错误或用户不存在'
    }

    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = auth.authenticate(username=username, password=password)

        if user is not None and user.is_active:
            auth.login(request, user)
            return HttpResponseRedirect(reverse('index'))
        else:
            return render(request, 'login.html', context=context)

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('login'))
