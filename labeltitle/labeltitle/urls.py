"""labeltitle URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from label.views import (
    index, login, logout, 
    reset, prev, next, 
    label, skip)

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^label/$', label, name='label'),
    url(r'^skip/$', skip, name='skip'),
    url(r'^reset/$', reset, name='reset'),
    url(r'^prev/$', prev, name='prev'),
    url(r'^next/$', next, name='next'),
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout'),
    path('admin/', admin.site.urls),
]
