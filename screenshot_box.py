# @Time     :   2021-3-26
# @Author   :   mikeyy
# @Desc     :   Used to draw bounding box for segments of screenshots, distribute dom node to corresponding segment


import os
import cv2
import json
import math
import functools
import numpy as np
import pandas as pd

# x1: 0, y1: 1, x2 : 2, y2 : 3
def rectint(blockA, blockB):
    # 矩形A左下角，右上角
    xa1 = blockA[0]
    ya1 = blockA[3]
    xa2 = blockA[2]
    ya2 = blockA[1]
    
    # 矩形B左下角，右上角
    xb1 = blockB[0]
    yb1 = blockB[3]
    xb2 = blockB[2]
    yb2 = blockB[1]
    
    ox = (xa1 + xa2) / 2
    oy = (ya1 + ya2) / 2
    ex = (xb1 + xb2) / 2
    ey = (yb1 + yb2) / 2
    
    intersected = ((abs(ex-ox) <= ((xa2-xa1)/2 + (xb2-xb1)/2)) and 
                  (abs(ey-oy) <= ((ya1-ya2)/2 + (yb1-yb2)/2)))
    
    if not intersected:
        return 0
    else:
        cx = max(xa1, xb1)
        cy = min(ya1, yb1)
        fx = min(xa2, xb2)
        fy = max(ya2, yb2)
        
        return abs(cx-fx) * abs(cy-fy)

def compare(n1, n2):
    # record['left'], record['top'], record['right'], record['bottom']
    xa1 = n1['node']['left']
    ya1 = n1['node']['top']
    
    xb1 = n2['node']['left']
    yb1 = n2['node']['top']
    
    if ya1 < yb1:
        return -1
    elif ya1 > yb1:
        return 1
    elif xa1 < xb1:
        return -1
    else:
        return 1

WORKING_ROOT = '/home/mikeyy/repo/webis-segment/web-segment-algo'
ENCODINGS = ['utf-8', 'gbk', 'EUC_KR', 'Shift_JIS', 'EUC_JP', 'ISO-2022-JP', 'KOI8-R']

documents = os.listdir(os.path.join(WORKING_ROOT, 'webis-webseg-20'))
#documents = ['000000']
documents.sort()
skip = False
skip_file = '001698'

for doc in documents:
    if doc != skip_file and skip:
        continue
    else:
        skip = False
    
    print('processing', doc)

    WORKING_DIR = os.path.join(WORKING_ROOT, 'webis-webseg-20', doc)
    img = cv2.imread(os.path.join(WORKING_DIR, 'screenshot.png'))
    with open(os.path.join(WORKING_DIR, 'ground-truth.json'), 'r') as f:
        ground_truth = json.load(f)
        segs = ground_truth['segmentations']['majority-vote']

    isClosed = True
    color    = (0, 255, 0)
    thickness= 5

    seg_obj = []
    i = 0
    for seg in segs:
        seg_obj.append(dict())
        seg_obj[len(seg_obj)-1]['poly'] = []
        
        x1 = 100000
        y1 = 100000
        x2 = -1
        y2 = -1
        for multi in seg:
            for poly in multi:
                
                for point in poly:
                    x1 = min(x1, point[0])
                    y1 = min(y1, point[1])
                    x2 = max(x2, point[0])
                    y2 = max(y2, point[1])
                 
        seg_obj[len(seg_obj)-1]['box'] = [x1, y1, x2, y2]
        cv2.putText(img, str(i), (int((x1+x2)/2), int((y1+y2)/2)), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 2)
        img = cv2.rectangle(img, (x1, y1), (x2, y2),  
                            color, thickness)
        i += 1
    cv2.imwrite(os.path.join(WORKING_DIR, 'screenshot-v.png'), img)

    isEncode = False
    for e in ENCODINGS:
    # read dom nodes
        try:
            nodes     = pd.read_csv(os.path.join(WORKING_DIR, 'nodes.csv'), encoding=e)
        except Exception as e:
            continue

        try:
             node_text = pd.read_csv(os.path.join(WORKING_DIR, 'nodes-texts.csv'), encoding=e)
        except Exception as e:
            continue
    
        if len(nodes) == 0 or len(node_text) == 0:
            print('skipping 0', doc)
            break

        isEncode = True
        break

    if not isEncode:
        print('skipping', doc)
        continue
    

    merge = nodes.join(node_text.set_index('xpath'), on='xpath')

    merge = merge.to_dict(orient='records')
    seg_basket = [[] for i in range(len(seg_obj))]

    for record in merge:
        if type(record['text']) == str or 'IMG' in record['xpath']:
            block = [record['left'], record['top'], record['right'], record['bottom']]
            scores = []
            for i in range(len(seg_obj)):
                s = rectint(block, seg_obj[i]['box'])
                scores.append((i, s))
            
            # sort
            scores.sort(key=lambda a: a[1], reverse=True)
            if len(scores) == 1 or scores[0][1] > scores[1][1]:
                seg_basket[scores[0][0]].append({'node' : record, 'scores' : scores})
            else:
                fitted = False
                index = -1
                for i in range(2):
                    for poly in seg_obj[scores[i][0]]['poly']:
                        if block == poly:
                            fitted = True
                            index = i
                            break
                if fitted:
                    seg_basket[scores[index][0]].append({'node' : record, 'scores' : scores})
                else:
                    seg_basket[scores[0][0]].append({'node' : record, 'scores' : scores})
    
    for i in range(len(seg_basket)):
        seg = seg_basket[i]
        seg.sort(key=functools.cmp_to_key(compare))
        seg.append({'box' : seg_obj[i]['box']})

    with open(os.path.join(WORKING_DIR, 'seg_info.json'), 'w+', encoding='utf-8') as f:
        f.write(json.dumps(seg_basket))

    img_path = os.path.join(WORKING_DIR, 'screenshot-v.png')
    img_v_path = os.path.join(WORKING_DIR, 'screenshot-' + doc + '.png')
    os.system('convert -resize 60%x60% ' + img_path + ' ' + img_v_path)
    os.system('rm ' + img_path)