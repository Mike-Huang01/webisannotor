import sqlite3
import csv

dbfile = './labeltitle/db.sqlite3'
conn = sqlite3.connect(dbfile)
cur = conn.cursor()

sql = '''
    SELECT page_id,  blocks, label_blocks, desc
    FROM label_page;
    '''

record_sql = '''
            SELECT page_id, block_id, xpath
            FROM label_record;
             '''

data = cur.execute(sql)

with open('./Page.csv', 'w+', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['page_id', 'blocks', 'label_blocks', 'desc'])
    writer.writerows(data)

records = cur.execute(record_sql)
with open('./Record.csv', 'w+', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['page_id', 'block_id', 'xpath'])
    writer.writerows(records)




